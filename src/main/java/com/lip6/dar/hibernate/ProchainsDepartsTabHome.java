package com.lip6.dar.hibernate;

// Generated Oct 19, 2014 1:28:30 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;


/**
 * Home object for domain model class ProchainsDepartsTab.
 * @see com.lip6.dar.hibernate.ProchainsDepartsTab
 * @author Hibernate Tools
 */
public class ProchainsDepartsTabHome {

	private static final Log log = LogFactory
			.getLog(ProchainsDepartsTabHome.class);

	private SessionFactory sessionFactory = null;

	
	public ProchainsDepartsTabHome(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}

	public void persist(ProchainsDepartsTab transientInstance) {
		log.debug("persisting ProchainsDepartsTab instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(ProchainsDepartsTab instance) {
		log.debug("attaching dirty ProchainsDepartsTab instance");
		try {
			Transaction trans=sessionFactory.getCurrentSession().beginTransaction(); 
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			trans.commit();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ProchainsDepartsTab instance) {
		log.debug("attaching clean ProchainsDepartsTab instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(ProchainsDepartsTab persistentInstance) {
		log.debug("deleting ProchainsDepartsTab instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ProchainsDepartsTab merge(ProchainsDepartsTab detachedInstance) {
		log.debug("merging ProchainsDepartsTab instance");
		try {
			ProchainsDepartsTab result = (ProchainsDepartsTab) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public ProchainsDepartsTab findById(int id) {
		log.debug("getting ProchainsDepartsTab instance with id: " + id);
		try {
			ProchainsDepartsTab instance = (ProchainsDepartsTab) sessionFactory
					.getCurrentSession().get(
							"com.lip6.dar.hibernate.ProchainsDepartsTab", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(ProchainsDepartsTab instance) {
		log.debug("finding ProchainsDepartsTab instance by example");
		try {
			List results = sessionFactory
					.getCurrentSession()
					.createCriteria(
							"com.lip6.dar.hibernate.ProchainsDepartsTab")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List getByStopDateTime(String stop, Date time) {
		log.debug("begin getByDateTime");
		try {
			
			Transaction trans = sessionFactory.getCurrentSession().beginTransaction(); 
			Query query = sessionFactory.getCurrentSession().createQuery("from ProchainsDepartsTab as p where p.idArea = :stop and p.timeDepart > :time order by p.timeDepart");
			query.setParameter("time", time);
			query.setParameter("stop", stop);
			query.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List results = query.list();
			trans.commit();
			log.debug("getByDateTime successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	
}
