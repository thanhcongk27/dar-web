package com.lip6.darweb.model;

import com.lip6.dar.hibernate.ProchainsDepartsTab;

public class Departure {
	private ProchainsDepartsTab depart;
	private String name;
	public ProchainsDepartsTab getDepart() {
		return depart;
	}
	public void setDepart(ProchainsDepartsTab depart) {
		this.depart = depart;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
