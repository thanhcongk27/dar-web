package com.lip6.darweb.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;
import org.hibernate.SessionFactory;

import com.lip6.dar.hibernate.LinesTab;
import com.lip6.dar.hibernate.LinesTabHome;
import com.lip6.dar.hibernate.ProchainsDepartsTab;
import com.lip6.dar.hibernate.ProchainsDepartsTabHome;
import com.lip6.dar.hibernate.StopAreasTab;
import com.lip6.dar.hibernate.StopAreasTabHome;
import com.lip6.darweb.model.Departure;
import com.opensymphony.xwork2.Action;

public class IndexAction implements Action, ServletContextAware {
	
		
	public String index() throws Exception {
		// get all stop_areas
		SessionFactory sf = (SessionFactory) ctx.getAttribute("SessionFactory");
		StopAreasTabHome stopDAO = new StopAreasTabHome(sf);
		List<StopAreasTab> stopList = stopDAO.getAll();
		if (null != stopList && stopList.size() > 0) {
			for (StopAreasTab stop : stopList) {
				String key = stop.getIdArea();
				String value = stop.getName() + ":" + key;
				areaMap.put(key, value);
				ctx.setAttribute("areaMap", areaMap);
			}
		}
		
		return SUCCESS;
	}
	
	public String execute() throws Exception {
		// get all stop_areas
		areaMap = (Map) ctx.getAttribute("areaMap");
		//toNameKey = (String) ctx.getAttribute("toNameKey");
		SessionFactory sf = (SessionFactory) ctx.getAttribute("SessionFactory");
		ProchainsDepartsTabHome prochainDAO = new ProchainsDepartsTabHome(sf);
		if (buttonName.equals("Prochain")){
			if (!"".equals(toNameKey)) {				
				List<ProchainsDepartsTab> prochainList = prochainDAO.getByStopDateTime(toNameKey, new Date());
				if (null != prochainList && prochainList.size() > 0) {
					departureList = new ArrayList<>();
					LinesTabHome lineDAO = new LinesTabHome(sf);
					for (ProchainsDepartsTab prochain : prochainList) {
						Departure d = new Departure();
						d.setDepart(prochain);
						LinesTab line = lineDAO.getByIdLine(prochain.getIdLine());
						if (null != line) {							
							d.setName(line.getCode());
							departureList.add(d);
						}
						
						
					}
				}
				return SUCCESS;
			} else {
				return SUCCESS;
			}
		}
		
		return SUCCESS; 
		
	}
		
	private ServletContext ctx;

	@Override
	public void setServletContext(ServletContext sc) {
		this.ctx = sc;
	}
	
	private Map areaMap = new HashMap();
	
	public Map getAreaMap () {		
		return areaMap;
	}	
	
	private String toName;

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}
	
	private String toNameKey;

	public String getToNameKey() {
		return toNameKey;
	}

	public void setToNameKey(String toNameKey) {
		this.toNameKey = toNameKey;
	}
	List<Departure> departureList;

	public List<Departure> getDepartureList() {
		return departureList;
	}

	public void setDepartureList(List<Departure> departureList) {
		this.departureList = departureList;
	}

	private String buttonName;

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}
	
	
	
}
