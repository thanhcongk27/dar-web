<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>    
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- Using Struts2 Tags in JSP --%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>


<html>
<head>
<sx:head />
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Home Page</title>
</head>
<body>
<h3>Welcome User</h3>

<s:form action="depart">			
	<sx:autocompleter name="toName" id="toId" list="areaMap"> </sx:autocompleter>   
	<s:submit value="OK" method="depart"></s:submit>
	
</s:form>
</body>
</html>